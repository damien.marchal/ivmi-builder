extends "res://addons/ivmi-builder/techniques/technique.gd"

@export var _camera_node_path: NodePath
@export var _controller_node_path: NodePath
@export var _arvrorigin_node_path: NodePath

@export var _mouse_sensitivity = 50.0
@export var _minLookAngleX =  -80.0
@export var _maxLookAngleX =  80.0

@onready var _camera = get_node(_camera_node_path)
@onready var _controller = get_node(_controller_node_path)
@onready var _arvrorigin = get_node(_arvrorigin_node_path)

var _mouseDelta = Vector3.ZERO
var _direction = Vector3.ZERO

var _nagivation_enabled = true

var _forward_pressed = false
var _backward_pressed = false
var _dead_zone = 0.125 # radius of the dead zone
var _dead_zone_squared = pow(_dead_zone, 2)

var _up = false
var _down = false
var _left = false
var _right = false

var _joystick_axis_x = 0;
var _joystick_axis_y = 0;

@export var _speed = 1

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	_controller.connect("button_pressed",Callable(self,"_on_Controller_button_pressed"))
	_controller.connect("button_released",Callable(self,"_on_Controller_button_release"))

func _physics_process(delta):
	_arvrorigin.position += _direction*_speed*delta

func _process(delta):
	
	_direction = Vector3.ZERO
	
	if _ivmi.is_2D():
		if _nagivation_enabled:
			_arvrorigin.rotation_degrees.x -= _mouseDelta.y * _mouse_sensitivity * delta
			_arvrorigin.rotation_degrees.x = clamp(_arvrorigin.rotation_degrees.x, _minLookAngleX, _maxLookAngleX)
			_arvrorigin.rotation_degrees.y -= _mouseDelta.x * _mouse_sensitivity * delta
			_mouseDelta = Vector2()

			if _up:
				_direction -= _camera.global_transform.basis.z.normalized()
			if _down:
				_direction += _camera.global_transform.basis.z.normalized()
			if _left:
				_direction -= _camera.global_transform.basis.x.normalized()
			if _right:
				_direction += _camera.global_transform.basis.x.normalized()
	else:
		_joystick_axis_x = _controller.get_joystick_axis(0);
		_joystick_axis_y = _controller.get_joystick_axis(1);
		if (pow(_joystick_axis_x,2) + pow(_joystick_axis_y,2) > _dead_zone_squared):
			_move_using_joystick(_joystick_axis_x,_joystick_axis_y)

func _move_using_joystick(dx,dy):
	pass

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.keycode==KEY_UP:
				_up = true
			if event.keycode==KEY_DOWN:
				_down = true
			if event.keycode==KEY_LEFT:
				_left = true
			if event.keycode==KEY_RIGHT:
				_right = true
			#if event.is_action("toggle_manipulation_mode"):
			#	_nagivation_enabled = false
			#	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			if event.keycode==KEY_UP:
				_up = false
			if event.keycode==KEY_DOWN:
				_down = false
			if event.keycode==KEY_LEFT:
				_left = false
			if event.keycode==KEY_RIGHT:
				_right = false
			#if event.is_action("toggle_manipulation_mode"):
			#	_nagivation_enabled = true
			#	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				
	if event is InputEventMouseMotion:
		_mouseDelta = event.relative

	
