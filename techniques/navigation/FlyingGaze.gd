extends "res://addons/ivmi-builder/techniques/navigation/Flying.gd"

func _move_using_joystick(dx,dy):
	_direction += _camera.global_transform.basis.x.normalized() * dx
	_direction -= _camera.global_transform.basis.z.normalized() * dy
