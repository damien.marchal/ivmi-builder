@tool
extends "res://addons/ivmi-builder/core/IvmiNode.gd"

enum DATA_TYPE {curve,array,string}



@export var active = true 

@export var _height_curve: Curve : set = _set_height_curve
@export var _color_curve: Curve : set = _set_color_curve
@export var _rotation_curve: Curve : set = _set_rotation_curve
@export var _speed_curve: Curve : set = _set_speed_curve
@export var _density_curve: Curve : set = _set_density_curve
@export var _brightness_curve: Curve : set = _set_brightness_curve
@export var _noisiness_curve: Curve : set = _set_noisiness_curve

var tunnel_param_data_script = preload("res://addons/ivmi-builder/techniques/control/Tunnel/TunnelParamData.gd")

func _ready() :
	super._ready()
	if _height_curve:
		_height_curve.connect("changed",Callable(self,"_on_height_curve_changed"))
	if _color_curve:
		_color_curve.connect("changed",Callable(self,"_on_color_curve_changed"))
	if _rotation_curve:
		_rotation_curve.connect("changed",Callable(self,"_on_rotation_curve_changed"))
	if _speed_curve:
		_speed_curve.connect("changed",Callable(self,"_on_speed_curve_changed"))
	if _density_curve:
		_density_curve.connect("changed",Callable(self,"_on_density_curve_changed"))
	if _brightness_curve:
		_brightness_curve.connect("changed",Callable(self,"_on_brightness_curve_changed"))
	if _noisiness_curve:
		_noisiness_curve.connect("changed",Callable(self,"_on_noisiness_curve_changed"))

func _process(delta):
	super._process(delta)

func _set_property(prop, vals) :
	super._set_property(prop, vals)

func get_tunnel_preset_data():
	var tunnel_param_data_array = []
	if _height_curve:
		var tunnel_param_data = tunnel_param_data_script.new()
		tunnel_param_data.name = "tunnel_height"
		tunnel_param_data.curve = _height_curve
		tunnel_param_data_array.append(tunnel_param_data)
		
	if _rotation_curve:
		var tunnel_param_data = tunnel_param_data_script.new()
		tunnel_param_data.name = "tunnel_rotation"
		tunnel_param_data.curve = _rotation_curve
		tunnel_param_data_array.append(tunnel_param_data)
		
	if _speed_curve:
		var tunnel_param_data = tunnel_param_data_script.new()
		tunnel_param_data.name = "tunnel_speed"
		tunnel_param_data.curve = _speed_curve
		tunnel_param_data_array.append(tunnel_param_data)
		
	if _color_curve:
		var tunnel_param_data = tunnel_param_data_script.new()
		tunnel_param_data.name = "tunnel_color_scale"
		tunnel_param_data.curve = _color_curve
		tunnel_param_data_array.append(tunnel_param_data)
		
	if _density_curve:
		var tunnel_param_data = tunnel_param_data_script.new()
		tunnel_param_data.name = "tunnel_density"
		tunnel_param_data.curve = _density_curve
		tunnel_param_data_array.append(tunnel_param_data)
		
	if _brightness_curve:
		var tunnel_param_data = tunnel_param_data_script.new()
		tunnel_param_data.name = "tunnel_brightness"
		tunnel_param_data.curve = _brightness_curve
		tunnel_param_data_array.append(tunnel_param_data)
		
	if _noisiness_curve:
		var tunnel_param_data = tunnel_param_data_script.new()
		tunnel_param_data.name = "tunnel_noisiness"
		tunnel_param_data.curve = _noisiness_curve
		tunnel_param_data_array.append(tunnel_param_data)
		
	return tunnel_param_data_array

func _update_tunnel():
	if get_parent():
		var parent = get_parent()
		if parent.has_method("_update_tunnel"):
			parent._update_tunnel()


func _set_height_curve(val):
	_height_curve = val
	_update_tunnel()
func _on_height_curve_changed() :
	_update_tunnel()

func _set_color_curve(val):
	_color_curve = val
	_update_tunnel()
func _on_color_curve_changed() :
	_update_tunnel()

func _set_rotation_curve(val):
	_rotation_curve = val
	_update_tunnel()
func _on_rotation_curve_changed() :
	_update_tunnel()

func _set_speed_curve(val):
	_speed_curve = val
	_update_tunnel()
func _on_speed_curve_changed() :
	_update_tunnel()

func _set_density_curve(val):
	_density_curve = val
	_update_tunnel()
func _on_density_curve_changed() :
	_update_tunnel()

func _set_brightness_curve(val):
	_brightness_curve = val
	_update_tunnel()
func _on_brightness_curve_changed() :
	_update_tunnel()	
	
func _set_noisiness_curve(val):
	_noisiness_curve = val
	_update_tunnel()
func _on_noisiness_curve_changed() :
	_update_tunnel()
