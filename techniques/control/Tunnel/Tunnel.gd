@tool
extends "res://addons/ivmi-builder/techniques/control/Tunnel/TunnelPreset.gd"

class_name Tunnel

#Current Tunnel preset, 0 being the default tunnel preset
@export var _preset_index = 0 : set = _set_preset_index
var _slices = []
var tunnel_preset_array = []
@onready var _area : Area3D = get_node("Area3D")

@onready var _nb_slices : int = $MultiSlice.multimesh.instance_count

const NUMBER_OF_SLICES : int = 100
const RADIUS : float = 0.1

func _ready():
	super._ready()
	_update_tunnel_preset_array()
	_update_multislice()

func _update_tunnel_preset_array():
	tunnel_preset_array = []
	tunnel_preset_array.append(get_tunnel_preset_data())
	var children = get_children()
	for child in children:
		if child.has_method("get_tunnel_preset_data"):
			if child.active:
				tunnel_preset_array.append(child.get_tunnel_preset_data())

func _update_multislice() :
	for s in range(_nb_slices) :
		var col : Color
		col.h = 0.5
		col.s = 1.0
		col.v = 0.5
		var trans : Transform3D = Transform3D()
		trans.origin = Vector3((s-_nb_slices/2)*(1.0/_nb_slices), 0, 0)
		var rot : Vector3 = Vector3(0.0, 0.0, PI/2.0)
		var sca : Vector3 = Vector3(0.9,1.0,1.0)
		var ratio : float = float(s) / float(_nb_slices)
		for param in tunnel_preset_array[_preset_index]:
			var value : float = param.curve.sample(ratio)	
			if value > 1.0 :
				value/=127
				
			match param.name:
				"tunnel_color_scale" :
					col = col.from_hsv(fmod(value*5.0,1.0), 1.0 - abs(value*2.0-1.0), value)
					pass
				"tunnel_height" :
					sca.y = value*0.9+0.1
					sca.z = value*0.9+0.1
				"tunnel_rotation" :
					rot.x = value*PI/4.0
				"tunnel_density" :
					sca.x = value*0.8+0.1
					pass
		
		trans.basis = Basis.from_euler(rot).scaled(sca)
		$MultiSlice.multimesh.set_instance_transform(s, trans)
		$MultiSlice.multimesh.set_instance_custom_data(s, Color(col.r,col.g,col.b,1.0))

func _process(delta : float) -> void	:
	super._process(delta)


func _physics_process(delta):
	if !Engine.is_editor_hint() :
		if _ivmi._pd_mode!=IvmiScene.PdMode.NONE: 
			var results = _area.get_overlapping_areas()
			if results.size()>0:
				for r in results:
					var col = r.get_parent()
					if col.is_in_group("ivmi_nodes"):
						#if collision, compute position ratio
						var col_pos = to_local(col.global_position)
						var ratio = clamp(col_pos.x+0.5,0,1)
						#set all active parameters
						for param in tunnel_preset_array[_preset_index]:
							var value : float = param.curve.sample(ratio)
							if value > 1.0 :
								value/=127
							col.set_property(param.name, [value])

func get_extent():
	return Vector3(1.0, 0.2, 0.2)

func _cycle_preset():
	_preset_index = (_preset_index+1)%tunnel_preset_array.size()
	_update_multislice()


func _set_property(prop, vals) :
	super._set_property(prop, vals)
	match prop:
		"triggered" :
			if vals[0]:
				_cycle_preset()

func _update_tunnel():
	_update_tunnel_preset_array()
	_update_multislice()
	
func _set_preset_index(val):
	if val < (get_child_count()-1) and val >= 0:
		_preset_index = val
		_update_multislice()
