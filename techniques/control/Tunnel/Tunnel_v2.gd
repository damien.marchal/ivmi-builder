tool
extends "res://addons/ivmi-builder/techniques/control/Tunnel/TunnelPreset.gd"

#Current Tunnel preset, 0 being the default tunnel preset
export var _preset_index = 0 setget _set_preset_index
var _slices = []
var tunnel_preset_array = []
onready var _area = get_node("Area")

var NUMBER_OF_SLICES = 50
var RADIUS = 0.1

func _ready():
	_update_tunnel_preset_array()
	_generate_slice()
	_update_slices()

func _update_tunnel_preset_array():
	tunnel_preset_array = []
	tunnel_preset_array.append(get_tunnel_preset_data())
	var children = get_children()
	for child in children:
		if child.has_method("get_tunnel_preset_data"):
			if child.active:
				tunnel_preset_array.append(child.get_tunnel_preset_data())

func _generate_slice():
	var sli = get_node("Slices")
	for s in range(0,NUMBER_OF_SLICES) :
		var new_slice = MeshInstance.new()
		new_slice.translation = Vector3((s-NUMBER_OF_SLICES/2)*
								(1.0/NUMBER_OF_SLICES), 0, 0)
		new_slice.mesh = CylinderMesh.new()
		new_slice.mesh.rings = 1
		new_slice.mesh.radial_segments = 5
		new_slice.mesh.top_radius=RADIUS
		new_slice.mesh.bottom_radius=RADIUS
		new_slice.mesh.height=1.0/NUMBER_OF_SLICES
		new_slice.rotation_degrees = Vector3(0,0,90)
		new_slice.set_surface_material(0, SpatialMaterial.new())
		new_slice.get_surface_material(0).params_cull_mode  = SpatialMaterial.CULL_BACK 
		sli.add_child(new_slice)
		_slices.push_back(new_slice)

func _update_slices() :
	var i=0.0
	for s in _slices :
		var ratio = i / _slices.size()
		s.get_surface_material(0).albedo_color.s=0.1
		s.get_surface_material(0).albedo_color.h=0.5
		s.get_surface_material(0).albedo_color.v=0.5
		s.scale.x = 0.5
		s.scale.z = 0.5
		s.rotation_degrees.x = 0
		for param in tunnel_preset_array[_preset_index]:
			var value
			if param.data_type == DATA_TYPE.array:
				value = _find_value_in_array(ratio,param.array)
			else:
				value = param.curve.interpolate(ratio)
				
			match param.name:
				"tunnel_color_scale" :
					s.get_surface_material(0).albedo_color.h = fmod(value*5.0,1.0)
					s.get_surface_material(0).albedo_color.s = 1.0 - abs(value*2.0-1.0)
					s.get_surface_material(0).albedo_color.v = value
				"tunnel_height" :
					s.scale.x = value*0.9+0.1
					s.scale.z = value*0.9+0.1
				"tunnel_rotation" :
					s.rotation_degrees.x = value*180
				"tunnel_density" :
					s.scale.y = value
		i+=1.0

func _physics_process(delta):
	if !Engine.editor_hint and _ivmi._pd_mode!=IvmiScene.PdMode.NONE: 
		var results = _area.get_overlapping_areas()
		if results.size()>0:
			for r in results :
				var col = r.get_parent()
				if col.is_in_group("ivmi_nodes"):
					#if collision, compute position ratio
					var col_pos = to_local(col.global_transform.origin)
					var ratio = col_pos.x+0.5
					#set all active parameters
					for param in tunnel_preset_array[_preset_index]:
						var value
						if param.data_type == DATA_TYPE.array:
							value = _find_value_in_array(ratio,param.array)
						else:
							value = param.curve.interpolate(ratio)
						col.set_property(param.name, [value])

func get_extent():
	return Vector3(1.0, 0.2, 0.2)

# Find the highest vec.x < x in the array
func _find_value_in_array(x,array):
	for vec in array:
		if x<=vec.x:
			return vec.y
	return array[array.size()-1].y

func _cycle_preset():
	_preset_index = (_preset_index+1)%tunnel_preset_array.size()
	_update_slices()


func _set_property(prop, vals) :
	._set_property(prop, vals)
	match prop:
		"triggered" :
			if vals[0]:
				_cycle_preset()

func _update_tunnel():
	_update_tunnel_preset_array()
	_update_slices()
	
func _set_preset_index(val):
	if val < (get_child_count()-1) and val >= 0:
		_preset_index = val
		_update_tunnel()
