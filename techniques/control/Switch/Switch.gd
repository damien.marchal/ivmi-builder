extends "res://addons/ivmi-builder/core/IvmiNode.gd"

@onready var _slider = get_node("Slider")
@onready var _ledOff = get_node("LedOff")
@onready var _ledOn = get_node("LedOn")
@onready var _on_position = get_node("PosOn").position
@onready var _off_position = get_node("PosOff").position
@export var switch_speed = 8

var _moving = false

@onready var base_albedo_v_led_On = _ledOn.get_surface_override_material(0).albedo_color.v
@onready var base_albedo_v_led_Off = _ledOff.get_surface_override_material(0).albedo_color.v


func _ready():
	_add_property("triggered", [0])
	_add_property("value", [0])
	# Turn the ledOff ON
	_ledOff.get_surface_override_material(0).albedo_color.v=base_albedo_v_led_Off + 50

func _physics_process(delta): 
	if _moving:
		if get_property("value")[0]:
			_slider.position = lerp(_slider.position,_on_position,delta * switch_speed)
			if (_slider.position-_on_position).length_squared()<0.00005:
				_ledOn.get_surface_override_material(0).albedo_color.v=base_albedo_v_led_On + 50
				_ledOff.get_surface_override_material(0).albedo_color.v=base_albedo_v_led_Off
				_moving = false
		else:
			_slider.position = lerp(_slider.position,_off_position,delta * switch_speed)
			if (_slider.position-_off_position).length_squared()<0.00005:
				_ledOn.get_surface_override_material(0).albedo_color.v=base_albedo_v_led_On
				_ledOff.get_surface_override_material(0).albedo_color.v=base_albedo_v_led_Off + 50
				_moving = false	

func _inverse_value(val):
	if val == 1:
		return 0
	else:
		return 1

func _set_property(prop, vals) :
	super._set_property(prop, vals)
	match prop:
		"triggered" :
			if vals[0]:
				set_property("value",[_inverse_value(get_property("value")[0])])
				print(get_property("triggered")[0])
				_moving = true
				

				
