extends IvmiNode

@onready var _button = get_node("Button")
@onready var _pressed_position = get_node("Pressed").position
@onready var _released_position = get_node("Released").position
@onready var initial_v = _button.get_surface_override_material(0).albedo_color.v
@export var release_speed = 8

var _moving = false

func _ready():
	super._ready()
	_add_property("triggered", [0])

func _process(delta):
	super._process(delta)

func _physics_process(delta): 
	if !get_property("triggered")[0] and _moving:
		_button.position = lerp(_button.position,_released_position,delta * release_speed)
		if (_button.position-_released_position).length_squared()<0.0005:
			_moving = false

func _set_property(prop, vals) :
	super._set_property(prop, vals)
	match prop:
		"triggered" :
			if vals[0]:
				_button.get_surface_override_material(0).albedo_color.v=initial_v + 50
				_button.position = _pressed_position
				_moving = true

			else:
				_button.get_surface_override_material(0).albedo_color.v=initial_v
