extends "res://addons/ivmi-builder/techniques/technique.gd"

@onready var _surface_mat = get_node("MeshInstance3D").get_surface_override_material(0)
@onready var _parent = get_parent()
@onready var _parent_scale = get_parent().scale



func _ready():
	_can_be_rotated = false
	_add_property("value", [0])

func set_property(prop, vals):
	super.set_property(prop, vals)
	match prop:
		"position":
			position.x = clamp(position.x,-_parent_scale.x,_parent_scale.x)
			position.y = 0
			position.z = 0
			_parent.set_property("value",[pos_to_val(position.x,_parent_scale.x)])
			update_color()
		"value":
			set_property("position",[val_to_pos(vals[0],_parent_scale.x),0,0])

#Convert position, ranging from -upper_lower_bound to +upper_lower_bound, to value ranging from 0 to 1
func pos_to_val(pos,upper_lower_bound):
	return (pos+upper_lower_bound)/(upper_lower_bound*2)
#Convert value ranging from 0 to 1,to position ranging from -upper_lower_bound to +upper_lower_bound
func val_to_pos(val,upper_lower_bound):
	return (val*upper_lower_bound*2)-upper_lower_bound

func update_color():
	_surface_mat.albedo_color.h= ((position.x + _parent_scale.x)/(_parent_scale.x*2))
