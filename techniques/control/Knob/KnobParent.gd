extends "res://addons/ivmi-builder/core/IvmiNode.gd"

@onready var _body = get_node("Body")

func _ready():
	_add_property("value",[0])
	_can_be_rotated = false

func _set_property(prop, vals) :
	super._set_property(prop, vals)
	match prop:
		"value":
			var _value = _body.get_property("value")
			if !(_value[0] == vals[0]):
				_body.set_property("value", vals)
